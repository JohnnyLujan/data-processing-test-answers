#!/bin/python

import h5py
import csv

f = h5py.File('madrid.h5', 'r')
#Plaza Castilla ID's https://www.kaggle.com/decide-soluciones/air-quality-madrid/version/5
idPlazaCastilla = '28079050'

with open('Plaza Castilla.csv','w') as out:
	csv_out=csv.writer(out)
	csv_out.writerow(list(f[idPlazaCastilla]['block0_items']))
	for row in list(f[idPlazaCastilla]['block0_values']):
		csv_out.writerow(row)