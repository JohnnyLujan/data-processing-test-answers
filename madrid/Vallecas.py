#!/bin/python

import h5py
import csv
import math

def store_dataset(file_name, dataset_name ,dataset_list, sum_dataset, porcent):
	with open(file_name,'w') as out:
		if len(dataset_list) != 0:
			sum_dataset /= len(dataset_list)
			print(sum_dataset)
			NO_ten_porcent = int(len(dataset_list)*porcent)
			print(NO_ten_porcent)
			dataset_list.sort(reverse = True)

			csv_out=csv.writer(out)
			csv_out.writerow([dataset_name])
			for item in dataset_list:
				if item>sum_dataset and NO_ten_porcent!=0:
					csv_out.writerow([item])
				else:
					break
				NO_ten_porcent -= 1
		else:
			csv_out=csv.writer(out)
			csv_out.writerow(['No existen registros'])

f = h5py.File('madrid.h5', 'r')
#Vallecas ID's https://www.kaggle.com/decide-soluciones/air-quality-madrid/version/5
idVallecas = '28079040'

NO_Average = 0.0
list_NO = []

NO_2_Average = 0.0
list_NO_2 = []

NOX_Average = 0.0
list_NOX = []

for row in list(f[idVallecas]['block0_values']):
	#nitric oxide level measured
	if not math.isnan(row[2]):
		NO_Average += row[2]
		list_NO.append(row[2])
	#nitrogen dioxide level measured
	if not math.isnan(row[3]):
		NO_2_Average += row[3]
		list_NO_2.append(row[3])
	#nitrous oxides level measured
	if not math.isnan(row[4]):
		NOX_Average += row[4]
		list_NOX.append(row[4])

store_dataset('Vallecas_NO.csv', 'NO', list_NO, NO_Average, 0.1)
store_dataset('Vallecas_NO_2.csv', 'NO_2', list_NO_2, NO_2_Average, 0.1)
store_dataset('Vallecas_NOX.csv', 'NOX', list_NOX, NOX_Average, 0.1)