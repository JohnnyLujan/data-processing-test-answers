#!/bin/python

import sqlite3
import requests
import json

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

conn = sqlite3.connect('USWildfires.sqlite')
conn.row_factory = dict_factory

c = conn.cursor()
for row in c.execute("SELECT * FROM Fires table1, (SELECT FOD_ID, FIRE_YEAR, MAX(FIRE_SIZE) FROM Fires GROUP BY FIRE_YEAR) table2 WHERE table1.FOD_ID=table2.FOD_ID"):
	r = requests.post("http://ptsv2.com/t/lp3vp-1531189579/post", data=row)
	print(r.status_code, r.reason)
