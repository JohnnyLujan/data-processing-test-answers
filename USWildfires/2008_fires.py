#!/bin/python

import sqlite3
import csv

conn = sqlite3.connect('USWildfires.sqlite')
c = conn.cursor()
with open('2008_fires.csv','w') as out:
	csv_out=csv.writer(out)
	for row in c.execute("SELECT * FROM Fires WHERE FIRE_YEAR=2008"):
		csv_out.writerow(row)